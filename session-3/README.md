# Setting Up GitLab with a Self-Signed Certificate

1. Getting familiar with the basics of SSL certificates is vital. I did some [basic reading](http://www.steves-internet-guide.com/ssl-certificates-explained/) to get a foundational understanding of what SSL certificates are and why they are important.
2. After the close reading, I created a certificate pair using `openssl`:
```
openssl req -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out nick.bradsevy.online.crt -keyout nick.bradsevy.online.key
```
3. With the certificate pair created, I could then [configure Gitlab](https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https) to use *https* with my self-signed certificate.