# RadLab

<table align="center"><tr><td align="center" width="9999">
<img src="/images/Logo.png" align="center" width="150" alt="Project icon">
</table>

# Scenario

Welcome to Vaperware, Inc., the maker of the best software never released. You have been hired to run their GitLab server. The old administrator left a while ago and did not leave any notes. Your job is to fix the below problems so Vaperware developers can start doing more DevOps better. There is an old, and grumpy, sysadmin who is available over Zoom chat to answer insitutional knowledge questions. He knows nothing about GitLab or this GitLab server.

1. GitLab doesn't start, start it and make sure it starts on reboot.
2. GitLab is not using HTTPS, make it use a self-signed certificate.
3. We don't know the password to log in as root to GitLab, reset it and log in to the web interface. Make sure registeration is off.
4. GitLab version is old, update it to newest, make sure not to break it along the way!
5. We want nightly backups sent to another server, script that. The target is nickbackup@git.technolibre.net, the root user on your server has an SSH key.
6. Fix any other problems you find along the way. Security policy prevents you from running a root shell, even through sudo, but you can run sudo commands.
