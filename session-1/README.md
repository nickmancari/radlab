# Getting GitLab Up and Running

1. Policy constraints had me add resources to the **/etc/sudoers** file anytime I needed to utilize something requiring `sudo`

2. Running a `gitlab-ctl start` yielded a number of status errors, pointing to a service not running. Had to start the service with `systemctl`

3. After getting gitlab service running with `gitlab-ctl start` we still saw **502** errors when visiting the instance in a browser.

4. A quick poke around with `top` showed we didn't have enough RAM. Because the VM only had *2 GB* of RAM we had to utilize swap to allocate more memory in order to start GitLab.

5. While configuring Swap, we noticed that the hard drive was full with `df`. A quick look at what was taking up the hard drive space with `du` showed a *large*
file in `/var/logs`. (big-ass-file.nick hehe)

6. Removing this file freed up enough disk space to continue the Swap configuration.

# Helpful Commands I Learned:

| Command   | Definition                                                | Use          |
|-----------|-----------------------------------------------------------|--------------|
| `whereis` | This sucker helps you find where things are in the system | `whereis rm` |
| `df`      | This tells you how much free disk space you have          | `df`         |
| `du`      | This tells you what all is using your disk space          | `du`         |
