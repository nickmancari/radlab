# Continue Getting GitLab Functional

1. Picking up where we left off last week, when the server was turned back on, we were still getting **502** error at the web UI.

2. Running a `gitlab-ctl status` revealed that `redis` was in a *down* state. This prompted a good time to look at logs. `gitlab-ctl tail` was helpful but was hard to read, so we took a look at the process-specific logs in `/var/logs/gitlab/redis`.

3. Navigating to this directory proved especially difficult with my current approach of using `cd` to put myself into the directory. While trying, I was met with a `Permission Denied`. My initial reaction from there was to use `sudo` to cd into the directory. This was the wrong approach. Through this I learned that `cd ` is a command that is, what is referred to as, a `builtin`. They are commands that are built into the shell and don't exist in the same way that system commands do. The solution here was to use `ls` to list the contents of the directory since we already have `sudo` privileges with it.

4. From there, we could access the `redis/current` file that contains recent log activity.The information contained here was insightful; pointing to an issue regarding *Unix sockets*, a foreign concept to me. Once this connection was made, the resulting error made sense; an old socket file was keeping the redis process from starting properly.

5. Checking where the default location of the socket file is located for Redis on GitLab, I sought to remove it. But, alas I couldn't delete the file *even as root*. The message `Operation Not Permitted` looked particular and after a quick Google search, I could determine that this behavior was a result of `chattr` settings. This keeps *anyone*, including root, from making changes to a file. Using, `lsattr` we could see which settings were applied to the file and remove them accordingly.

6. Once the `chattr` settings were removed, we could then delete the socket file and restart Redis.

7. When Redis came up the **502** errors ceased. I could see from the web UI a proper login screen on our instance.


# Helpful Linux Commands I Learned:

| Command   | Definition                                                                                   | Use                               |
|-----------|----------------------------------------------------------------------------------------------|-----------------------------------|
| `chattr`  | Tool used to change file attributes, such as keeping anyone from making changes to a file    | `chattr <attribute> example.file` |
| `builtin` | Shell builtin commands are commands that can be executed within the running shell's process. |                                   |

<br><br>
# :rotating_light: Important Linux Ideology :rotating_light:

*Everything is a file.*